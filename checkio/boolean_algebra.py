OPERATION_NAMES = ("conjunction", "disjunction", "implication", "exclusive", "equivalence")

def boolean(x, y, operation):
	if operation == OPERATION_NAMES[0]: 
		# conjunction
		return (x & y)
	elif operation == OPERATION_NAMES[1]: 
		# disjunction
		return (x | y)
	elif operation == OPERATION_NAMES[2]: 
		# implicationt
		return ((not x) | y)
	elif operation ==  OPERATION_NAMES[3]: 
		# exclusive
		return (x ^ y)
	elif operation ==  OPERATION_NAMES[4]: 
		# equivalence
		return (x == y)

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert boolean(1, 0, u"conjunction") == 0, "and"
    assert boolean(1, 0, u"disjunction") == 1, "or"
    assert boolean(1, 1, u"implication") == 1, "material"
    assert boolean(0, 1, u"exclusive") == 1, "xor"
    assert boolean(0, 1, u"equivalence") == 0, "same?"
