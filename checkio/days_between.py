MONTHS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def days_diff(date1, date2):
    """
        Find absolute diff in days between dates
    """
    years = 0
    max_years = max(date1[0], date2[0])
    min_years = min(date1[0], date2[0])
    for x in range(min_years, max_years):
        if x % 4 == 0:
            # leap year
            years += 366
        else:
            years += 365
    max_month = max(date1[1], date2[1])
    min_month = min(date1[1], date2[1])
    month = 0
    for x in range (min_month, max_month):
        if
        month += MONTHS[x - 1]
    days = abs(date2[2] - date1[2])
    return years + month + days

if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert days_diff((1982, 4, 19), (1982, 4, 22)) == 3
    assert days_diff((2014, 1, 1), (2014, 8, 27)) == 238
    assert days_diff((2014, 8, 27), (2014, 1, 1)) == 238