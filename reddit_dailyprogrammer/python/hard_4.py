__author__ = 'ashleyng'

import sys
import re
import itertools


def test_math(permuations):
    for element in permutation:
        first_num = int(element)
        second_num = int(permutation[1])
        third_num = int(permutation[2])

    if first_num + second_num == third_num:
        return '+'
    elif first_num - second_num == third_num:
        return '-'
    elif first_num * second_num == third_num:
        return '*'
    elif first_num / second_num == third_num:
        return '/'
    return '-1'

if __name__ == "__main__":
    # get file name
    filename = sys.argv[1]
    file = open(filename, 'r')

    with file as f:
        for line in f:
            # get rid of newline
            content = line.strip(" \n")
            # strore line in array
            content = line.split()
            # print content
            permutation = itertools.permutations(content, 3)
            for perm in permutation:
                function = test_math(perm)
                if function != '-1':
                    print perm[0] + function + perm[1] + '=' + perm[3]


