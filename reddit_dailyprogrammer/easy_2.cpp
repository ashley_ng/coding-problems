#include <iostream>
using namespace std;

/* 
	the challenge is to create some form of calculator
	Can be a tip calculator or a F = M * A and etc
*/

void calculateAndPrint(int percent, double billTotal) {
	double tip = billTotal * (percent / 100.0);
	double total = tip + billTotal;
	printf("%d%%: $%.2f, bill+tip: $%.2f\n", percent, tip, total);
}

int main(int argc, char *argv[]) {
	double billTotal; 
	
	while (cin.good()) {
		cout << "enter amount of bill: $";
		cin >> billTotal;
		
		if (cin.good()) {
			calculateAndPrint(15, billTotal);
			calculateAndPrint(18, billTotal);
			calculateAndPrint(20, billTotal);
		}
		
	}
	
	
		 
}