#include <iostream>
#include <stdlib.h>

using namespace std;

/*
	the challenge was to make a program emulate "guessing game"
	but with the roles reveresed, i.e. the human chooses a number and
	the computer tries to guess
*/

// 0: does not equal
// 1: equals
// 2: too high
// 3: too low

// breaks if you enter char
int main(int argc, char *argv[]) {
	int upperBound = 100;
	int lowerBound = 1;
	int number = 50;
	
	int response = 0;
	
	while (response != 1) {
		cout << "is your number " << number << " ?" << endl;
		cout << "1: that's your number\n2: too HIGH\n3: too LOW" << endl;
		cin >> response;
		
		if (response == 2) {
			upperBound = number;
			number = number -  ((upperBound - lowerBound)/2);
		}
		else if (response == 3) {
			lowerBound = number;
			number = number + ((upperBound - lowerBound)/2);
		}
		else {
			cout << "PLEASE ENTER A VALID ENTRY" << endl;
		}
	}
}