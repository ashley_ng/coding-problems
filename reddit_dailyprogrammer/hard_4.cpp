#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

/*
    the challenge was to find the math operation that corresponded
    with 3 given numbers. I've updated requirements a bit to handle
    entering numbers from stdin.
    The first number given will be the number of lines
    then each line will have 3 numbers.
 */

void print_solution(int numbers[], string operation){
    cout << numbers[0] << " " << operation << " " << numbers[1] << " = " << numbers[2] << endl;
}

int main(int argc, char *argv[]){
    int num_lines;
    cin >> num_lines;
    for(int x = 0; x < num_lines; x++){
        int numbers[3] = {};
        cin >> numbers[0];
        cin >> numbers[1];
        cin >> numbers[2];

        do {
            if(numbers[0] + numbers[1] == numbers[2]){
                print_solution(numbers, "+");
                break;
            }
            else if(numbers[0] - numbers[1] == numbers[2]){
                print_solution(numbers, "-");
                break;
            }
            else if(numbers[0] * numbers[1] == numbers[2]){
                print_solution(numbers, "*");
                break;
            }
            else if(numbers[0] / numbers[1] == numbers[2]){
                print_solution(numbers, "/");
                break;
            }
            else if(numbers[0] % numbers[1] == numbers[2]){
                print_solution(numbers, "%");
                break;
            }
        } while(next_permutation(numbers, numbers+3));

    }
}