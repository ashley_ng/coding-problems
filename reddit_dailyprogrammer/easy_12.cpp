#include <iostream>
using namespace std;

/*
    challenge was to print all permutations of a string given
    The instructions say that it takes a string, it didn't specify whether
    it was a single word or not, but the current got handles multiple words in the string
    I decided to take in the word then store it in a char array because I felt that it would be
    easies to use the built in permutation function then to write my own permutation function
 */

int main(int argc, char *argv[]) {
    string word;

    getline(cin, word);
    int word_size = word.size();
    char word_array[word_size];

    for(int x = 0; x < word_size; x++){
        word_array[x] = word[x];
    }

    do{
        for(int x = 0; x < word_size; x++){
            cout << word_array[x];
        }
        cout << endl;
    }while(next_permutation(word_array, word_array+word_size));
    return 0;
}