#include <iostream>
#include <vector>
using namespace std;

/*
    challenge is, given a number, factor the number
 */

bool is_prime(int number){
    if(number%2 != 0 && number%3 != 0){
        return true;
    }
    return false;
}

void print_vector(vector<int> numbers){
    for(vector<int>::iterator it = numbers.begin(); it != numbers.end(); it++){
        cout << *it << " ";
    }
    cout << endl;
}

int main(int argc, char *argv[]) {

    int number;
    vector<int> factors;
    cin >> number;
    while(!is_prime(number)){
        if(number%2 == 0){
            number /= 2;
            factors.push_back(2);
        }
        else if(number%3 == 0){
            number /= 3;
            factors.push_back(3);
        }
    }
    if (number!=1){factors.push_back(number);}

    print_vector(factors);

    return 0;
}