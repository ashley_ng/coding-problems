#include <iostream>
#include <string>

using namespace std;

/*
	The challenge is to make program that prompts the user for their
	name, age, and reddit username. Then it will display all the given information
	in a sentance
*/
int main(int argc, char *argv[]) {
	string name;
	int age;
	string username;
	
	cout << "What is your name: ";
	cin >> name;
	
	cout << "How old are you? ";
	cin >> age;
	
	cout << "What is your reddit username? ";
	cin >> username;
	
	cout << "your name is " << name << ", you are " << age << " years old, and your username is " << username << endl;
}