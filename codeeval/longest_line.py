# Sample code to read in test cases:

import sys
test_cases = open(sys.argv[1], 'r')
data = test_cases.readlines()[1:]
data = sorted(data, key=lambda word:len(word), reverse=True)
print "\n".join(data[:2])


test_cases.close()