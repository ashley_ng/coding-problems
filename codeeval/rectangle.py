# Sample code to read in test cases:

import sys

def overlap(rect1, rect2):
	if (rect1[0] < rect2[2]) and (rect1[1] > rect2[3]) and (rect2[0] > rect1[2]) and (rect2[1] > rect1[3]):
		return True
	return False

test_cases = open(sys.argv[1], 'r')
for test in test_cases:
    test = test.strip().split(',')
    rect1 = test[:len(test)/2]
    rect2 = test[len(test)/2:]
    print overlap(rect1, rect2)
    

test_cases.close()
