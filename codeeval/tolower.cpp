// Sample code to read in test cases:
#include <iostream>
#include <fstream>
using namespace std;
int main(int argc, char *argv[]) {
    ifstream stream(argv[1]);
    string line;
    while (getline(stream, line)) {
    	for (int x = 0; x < line.length(); x++) {
        	cout << (char)tolower(line[x]);
    	}
    	cout << "\n";
    }
    // cout << endl;
    return 0;
} 