# Sample code to read in test cases:

import sys
import itertools

test_cases = open(sys.argv[1], 'r')
for word in test_cases:
    print ",".join(sorted(["".join(p) for p in itertools.permutations(word.strip())]))

test_cases.close()
