
#include <iostream>
#include <fstream>
using namespace std;
int main(int argc, char *argv[]) {
    int num; 
    int sum = 0;
    cin >> num;
    while (cin.good()) {
    	sum += num;
    	cin >> num;
    }
    cout << sum;
    return 0;
} 