#include <stdio.h>
#include <iostream>
#include <math.h>
using namespace std;

bool isPrime(int num) {
	for (int i = 2; i <= sqrt(num); ++i)
	{		
		if (num % i == 0)
		{
			return false;
		}
	}
	return true;

}

int main(int argc, char const *argv[])
{
	int sum = 0;
	int count = 0;
	int num = 1;
	while (count <= 1000)
	{
		bool is_prime = isPrime(num);
		if (is_prime)
		{
			sum += num;
			count++;
		}
		num++;
	}
	cout << sum << endl;
	return 0;
}