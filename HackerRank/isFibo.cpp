#include <iostream>
#include <vector>

using namespace std;

bool isFibo(int n) {
	int prev = 0;
	int cur = 1;
	while (true) {
		prev = cur;
		cur = prev + cur;
		
		if (cur == n) {
			return true;
		}
		else if (cur > n) {
			return false;
		}
		
	}
	return false;
}

int main(int argc, char *argv[]) {
	int numNumbers;
	int number;
	bool fibo;
	
	cin >> numNumbers;
	
	while (numNumbers > 0) {
		cin >> number;
		fibo = isFibo(number);
		if (fibo) {
			cout << "isFibo" << endl;
		}
		else {
			cout << "isNotFibo" << endl;
		}
		
		numNumbers--;
	}
}