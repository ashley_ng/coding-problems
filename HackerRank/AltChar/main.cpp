#include <iostream>
#include <string>

using namespace std;
int main(int argc, char *argv[]) {
	
	int num_lines;
	string line;
	
	cin >> num_lines;
	cin.get(); // handle newline after num_lines
	for(int x = 0; x < num_lines; x++) {
		getline(cin, line);
		char prev = line[0]; 
		char current = ' ';
		int deletes = 0;
		for(int x = 1; x < line.size(); x++){
			current = line[x];
			deletes = current == prev ? deletes + 1 : deletes;
			prev = current;
		}
		cout << deletes << endl;
		deletes = 0;
	}
}