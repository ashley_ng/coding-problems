#include <iostream>
#include <vector>

using namespace std;

void print_vector(vector<int> ar){
	for (vector<int>::iterator it = ar.begin(); it != ar.end(); it++) {
		cout << *it << " ";
	}
	cout << endl;
}


void insertionSort(vector <int>  ar) {
	int number = ar[ar.size() - 1];
	// ignore last element, end - 2
	for (int x = ar.size() - 2; x >= -1; x--) {
		if(x == -1){
			ar[x + 1] = number;
			print_vector(ar);
			break;
		}
		if(ar[x] > number){
			ar[x + 1] = ar[x];
			print_vector(ar);
		}
		else {
			ar[x + 1] = number;
			print_vector(ar);
			break;
		}
	}

}


int main(void) {
	vector <int>  _ar;
	int _ar_size;
	cin >> _ar_size;
	for(int _ar_i=0; _ar_i<_ar_size; _ar_i++) {
	   int _ar_tmp;
	   cin >> _ar_tmp;
	   _ar.push_back(_ar_tmp); 
	}

	insertionSort(_ar);
   
	return 0;
}