#include <iostream>

using namespace std;

const int NUM_NUMBERS = 99;

int* counts(int numbers[], int size){
	int* counts[NUM_NUMBERS] = {0};
	
	for(int x = 0; x < size; x++){
		counts[numbers[x]]++;
	}
	return counts;
}



int main(int argc, char *argv[]) {
	int list_size;
	
	cin >> list_size;
	
	int numbers[list_size];
	
	for (int x = 0; x < list_size; x++) {
		cin >> numbers[x];
	}
	int counts[NUM_NUMBERS] = counts(numbers);
	
}