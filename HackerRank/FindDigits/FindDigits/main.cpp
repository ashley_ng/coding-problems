//
//  main.cpp
//  FindDigits
//
//  Created by Ashley Ng on 11/15/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

#include <iostream>
#include <map>
using namespace std;

int main(int argc, const char * argv[]) {
    // insert code here...
    int startNum;
    int rollingNum;
    
    int lines;
    
    cin >> lines;
    
    while (lines > 0) {
        //read in number
        // start dividing
        int count = 0;
        cin >> startNum;
        rollingNum = startNum;
        while (rollingNum > 0) {
            int tens = rollingNum % 10;
            rollingNum /= 10;
            if (tens == 0) {
                continue;
            }
            count = startNum % tens == 0 ? count + 1 : count;
        }
        cout << count << endl;
        lines--;
        
        
//        int count = 0;
//        cin >> startNum;
//        rollingNum = startNum;
//        while (rollingNum > 0) {
//            int tens = rollingNum % 10;
//            rollingNum /= 10;
//            count = startNum % tens == 0 ? count+1 : count;
//        }
//        cout << count << endl;
//        lineCount++;
        
    }
    return 0;
}
