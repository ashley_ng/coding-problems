#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    
    int test_cases;
    int n;
    int m;
    int sum;
    cin >> test_cases;

    for (int i = 0; i < test_cases; ++i)
    {
    	cin >> n;
    	cin >> m;
    	sum = 0;
    	for (int x = 1; x <= n; ++x)
    	{
    		sum += x % m;
    	}
    	cout << sum << endl;
    }

    return 0;
}
