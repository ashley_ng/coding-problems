//
//  main.cpp
//  UtopianTree
//
//  Created by Ashley Ng on 11/15/14.
//  Copyright (c) 2014 Ashley Ng. All rights reserved.
//

#include <iostream>
using namespace std;

int height(int n) {
    int height = 1;
    int count = 1;
    while (count <= n) {
        if (count % 2 != 0) {
            height *= 2;
        }
        else {
            height++;
        }
        count++;
    }
    
    return height;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    int T;
    cin >> T;
    while (T--) {
        int n;
        cin >> n;
        cout << height(n) << endl;
    }
    return 0;
}
