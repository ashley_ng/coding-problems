#include <iostream>
#include <map>
#include <stdio.h>
#include <ctype.h>

using namespace std;

bool test_pangram(string line){
	char alphabet;
	map<char, int> alphabets;
	for(int x = 0; x < line.size(); x++){
		alphabet = tolower(line[x]);
		if(alphabets.count(alphabet) == 0 && (((int)alphabet >= 97) && (int)alphabet <= 127)){
			alphabets.insert(pair<char, int>(alphabet, 1));
		}
	}
	
	for (map<char, int>::iterator it = alphabets.begin(); it != alphabets.end(); it++) {
		cout << it->first << " " << it->second << endl;
	}
	
	if(alphabets.size() == 26){
		return true;
	}
	return false;
}


int main(int argc, char *argv[]) {
	string line;
	
	while(cin.good()){
		getline(cin, line);
		if(line.size() < 26){
			cout << "not pangram" << endl;
		}
		else{
			bool is_pangram = test_pangram(line);			if (is_pangram == true){
				cout << "pangram" << endl;
			}
			else {
				cout << "not pangram" << endl;
			}
		}
	}
}