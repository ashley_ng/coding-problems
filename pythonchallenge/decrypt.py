from string import maketrans
offset = 2

def decrypt(string):
	for char in string:
		if char.isalpha():
			ascii = ord(char)
			ascii += offset
			if ascii > 122:
				ascii = ascii - 26
			sys.stdout.write(chr(ascii))
		else:
			sys.stdout.write(char)

def easy_decrypt(str):
	intab = "abcdefghijklmnopqrstuvwxyz"
	outtab = "cdefghijklmnopqrstuvwxyzab"
	table = maketrans(intab, outtab)
	print str.translate(table)


string = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."

easy_decrypt(string)

# intab = "koe"
# outtab = "mqg"
# trantab = maketrans(intab, outtab)

# str = "this is string example....wow!!!";
# print string.translate(trantab);